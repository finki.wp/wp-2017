package mk.ukim.finki.wp.videoaggregator.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import mk.ukim.finki.wp.videoaggregator.web.serializers.TagListSerializer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Riste Stojanov
 */
@Indexed
@AnalyzerDef(name = "emtAnalyser",
  tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
    filters = {
    @TokenFilterDef(factory = LowerCaseFilterFactory.class)
  })
@Entity
@NamedEntityGraph(
  name = "loadAll",
  attributeNodes = {
    @NamedAttributeNode("category"),
    @NamedAttributeNode("tags")
  })
@Table(name = "videos")
public class Video {

  @Id
  @GeneratedValue
  public Long id;

  @Field(index = org.hibernate.search.annotations.Index.YES, store = Store.NO, analyze = Analyze.YES)
  @Analyzer(definition = "emtAnalyser")
  @Boost(2f)
  @Column(name = "my_title")
  public String title;


  @Field(index = org.hibernate.search.annotations.Index.YES, store = Store.NO, analyze = Analyze.YES)
  @Analyzer(definition = "emtAnalyser")
  @Boost(0.5f)
  public String description;

  public String url;

  @IndexedEmbedded
  @ManyToOne
  @JoinColumn(name = "my_category_id")
  public Category category;


  @IndexedEmbedded
  @JsonSerialize(using = TagListSerializer.class)
  @ManyToMany()
  @JoinTable(name = "join_table")
  public List<Tag> tags = new ArrayList<>();


  @JsonProperty("category")
  public Long categoryTitle() {
    return this.category != null ? category.id : null;
  }

}
