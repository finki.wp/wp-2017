package mk.ukim.finki.wp.videoaggregator.model.exceptions;

/**
 * @author Riste Stojanov
 */
public class DuplicateVideoTag extends RuntimeException {
}
