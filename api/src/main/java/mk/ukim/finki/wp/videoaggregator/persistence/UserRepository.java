package mk.ukim.finki.wp.videoaggregator.persistence;

import mk.ukim.finki.wp.videoaggregator.model.User;

import java.util.Optional;

public interface UserRepository {

  Optional<User> findByEmail(String email);

  User save(User user);

  User findByUsername(String username);

  User findById(Long id);

  long count();
}
