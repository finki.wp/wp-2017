package mk.ukim.finki.wp.videoaggregator.persistence.jpa;

import mk.ukim.finki.wp.videoaggregator.model.UserVerificationToken;
import mk.ukim.finki.wp.videoaggregator.persistence.VerificationTokenRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

@Profile("jpa")
public interface JpaVerificationTokenRepository extends VerificationTokenRepository, JpaRepository<UserVerificationToken, Long> {

}
