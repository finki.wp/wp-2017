package mk.ukim.finki.wp.videoaggregator.service;

/**
 * Created by ristes on 7/21/15.
 */
public interface MessageSendingService {

  void send(String topic, Object data, String... topicParams);

  void sendToUser(String user, String topic, Object data, String... topicParams);
}
