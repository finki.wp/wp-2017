package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.service.LastNameProvider;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Riste Stojanov
 */
@Primary
@Service
public class StaticMapLastNameProvider implements LastNameProvider {

    private Map<String, String> map = new HashMap<>();

    public StaticMapLastNameProvider() {
        map.put("Riste", "Stojanov");
        map.put("Dimitar", "Trajanov");
    }

    @Override
    public String lastName(String name) {
        return map.get(name);
    }
}
