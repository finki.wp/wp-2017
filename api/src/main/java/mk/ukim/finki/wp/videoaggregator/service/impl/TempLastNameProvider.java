package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.service.LastNameProvider;
import org.springframework.stereotype.Service;

/**
 * @author Riste Stojanov
 */
@Service
public class TempLastNameProvider implements LastNameProvider {
    @Override
    public String lastName(String name) {
        return "Stojanov";
    }
}
