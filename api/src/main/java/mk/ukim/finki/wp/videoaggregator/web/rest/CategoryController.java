package mk.ukim.finki.wp.videoaggregator.web.rest;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * @author Riste Stojanov
 */
@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping(value = "/api/categories", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class CategoryController {

  private final CategoryService categoryService;

  @Autowired
  public CategoryController(CategoryService categoryService) {
    this.categoryService = categoryService;
  }

  @RequestMapping("/by_title")
  public Iterable<Category> findByTitle(@RequestParam String title) {
    return categoryService.findByTitle(title);
  }

  @RequestMapping("/by_title_like")
  public Iterable<Category> findByTitleLike(@RequestParam String title) {
    return categoryService.findByTitleLike(title);
  }

  @RequestMapping(method = RequestMethod.GET)
  public Iterable<Category> all() {
    return categoryService.findAll();
  }

  @RequestMapping(value = "/save", method = RequestMethod.POST)
  public Category save(@RequestBody Category category){
    return categoryService.save(category);
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public void delete(@PathVariable Long id){
    categoryService.delete(id);
  }

}
