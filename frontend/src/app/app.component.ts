import {Component, OnDestroy, OnInit} from '@angular/core';
import {Video} from '../model/Video';
import {VideoServiceInterface} from './services/VideoServiceInterface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  title = 'Video Aggregator';
  public videos: Video[];

  constructor(private videoService: VideoServiceInterface) {

  }

  private observable;

  ngOnInit(): void {
    this.observable = this.videoService.load();
    this.observable.subscribe(videos => {
      console.info('in component: ', videos)
      this.videos = videos;
    });
  }


}
