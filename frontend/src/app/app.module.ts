import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {VideoService} from './services/video/video.service';
import {VideoDetailsComponent} from './video-details/video-details.component';
import {VideoEditComponent} from './video-edit/video-edit.component';
import {FormsModule} from '@angular/forms';
import {VideoTitlePipe} from './pipes/video-title.pipe';
import {SanitizePipe} from './pipes/sanitize.pipe';
import {FilterPipe} from './pipes/filter.pipe';
import {IsUrlDirective} from './directives/is-url/is-url.directive';
import {StringifyPipe} from './pipes/stringify.pipe';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http'
import {VideoServiceInterface} from "./services/VideoServiceInterface";
import {VideoHttpService} from "./services/video-http.service";


@NgModule({
  declarations: [
    AppComponent,
    VideoDetailsComponent,
    VideoEditComponent,
    VideoTitlePipe,
    SanitizePipe,
    FilterPipe,
    IsUrlDirective,
    StringifyPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    VideoHttpService,
    VideoService,
    {provide: VideoServiceInterface, useClass: VideoHttpService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
